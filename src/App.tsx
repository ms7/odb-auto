import React, { useState } from 'react';

interface Response {
  id: number,
  number: string,
  model: string,
  year: string,
  date: string,
  registration: string,
  capacity: number,
  ownerHash: string,
  color: string,
  kind: string,
  body: string,
  ownWeight: number,
  regAddrKoatuu: number,
  depСode: number,
  dep: string,
  forDevelopers: string
  }



function App() {
  const [query, setQuery] = useState('');
  const [result, setResult] = useState<null | Response>(null);
  const [error, setError] = useState<null | string>();

  const check = async () => {
    try {
      setError(null)
      const response = await fetch(`https://opendatabot.com/api/v3/public/transport?number=${query.toUpperCase()}`);
      setResult(await response.json())

    } catch(e) {
      setError('Відбулась помилка')

    }
    

  }



  return (
    <>
    <div>
      <input type="text" value={query} onChange={e => setQuery(e.target.value)} />
    </div>
    <div>
      <button onClick={check}>Перевіриити</button>
    </div>
    { error ? <div style={{backgroundColor: 'red', color: 'white'}}>{error}</div> : 
      result ? 
    <dl>
      <dt>Номер</dt>
      <dd><b>{result.number}</b></dd>
      <dt>Виробник</dt>
      <dd><b>{result.model}</b></dd>
      <dt>Рік</dt>
      <dd><b>{result.year}</b></dd>
      <dt>Дата реєстрації</dt>
      <dd><b>{result.date}</b></dd>
      <dt>Тип реєстрації</dt>
      <dd><b>{result.registration}</b></dd>
      <dt>Об'єм двигуна</dt>
      <dd><b>{result.capacity}</b></dd>
      <dt>Колір</dt>
      <dd><b>{result.color}</b></dd>
      <dt>Тип автомобіля</dt>
      <dd><b>{result.kind}</b></dd>
      <dt>Тип кузова</dt>
      <dd><b>{result.body}</b></dd>
      <dt>Маса</dt>
      <dd><b>{result.ownWeight}</b></dd>
      <dt>Код АТУ</dt>
      <dd><b>{result.regAddrKoatuu}</b></dd>
      <dt>Код ТСЦ</dt>
      <dd><b>{result.depСode}</b></dd>
      <dt>ТСЦ</dt>
      <dd><b>{result.dep}</b></dd>
    </dl> : null
    }
    </>
  );
}

export default App;
